"""Вспомогательный файлик, в котором лежат всякие вспомогательные функции
"""
import json


def is_json(myjson):
    """Проверяет является ли строка на входе - строкой содержашей Json
    """
    try:
        json_object = json.loads(myjson)
    except ValueError as e:
        return False
    return True
