"""Файл содержит описание моделей
"""

from django.db import models


class Group(models.Model):
    """оказалось, что Sqlite3 не поддерживает, массивы из "choices",
        по этому пришлось лепить связи "многие-ко-многим.
        Соответственно это можель для групп"
    """

    str_id = models.CharField(max_length=3)
    name = models.CharField(max_length=30)


class Color(models.Model):
    """оказалось, что Sqlite3 не поддерживает, массивы из "choices",
        по этому пришлось лепить связи "многие-ко-многим.
        Соответственно это можель для цветов"
    """
    str_id = models.CharField(max_length=3)
    name = models.CharField(max_length=30)


class Inserts(models.Model):
    """Основная модель, содержащая в себе поля объекта "вставка"
    """

    name = models.CharField(max_length=60)  # - название - строка, кириллица
    groups = models.ManyToManyField(Group)  # - группы (категории вставки) - список (id : group_name)
    colors = models.ManyToManyField(Color)  # - цвета - список (id : color_name)
    weight = models.FloatField()  # - вес в граммах - 2 точки после запятой
    hard = models.IntegerField()  # - твёрдость по Моосу - целое число, от 0 до 10
    place = models.CharField(max_length=60)  # - место происхождения - строка, кириллица
