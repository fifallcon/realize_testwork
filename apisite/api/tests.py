from django.test import TestCase, Client
from api.models import Inserts, Color, Group
from django.contrib.auth.models import User
from api.utils import is_json
import base64


class InsertsModelTest(TestCase):
    """тесты для проверки наличия хотябы одного цвета
    """
    
    @classmethod
    def setUpTestData(cls):
        """инициализация данных
        """

        # создаём 1 цвет
        colors = Color.objects.create(
            str_id="666",
            name="test_color"
        )
        # создаём 1 группу
        group = Group.objects.create(
            str_id="666",
            name="test_group"
        )

        # создаём первую вставку
        ins1 = Inserts.objects.create(
            str_id="666",
            name="test1",
            weight=55.5,
            hard=55,
            place="Тагил"
        )
        # Добавляем ей, группы и цвета
        ins1.colors.add(colors)
        ins1.groups.add(group)

        # создаём 2ую вставку
        ins2 = Inserts.objects.create(
            str_id="667",
            name="test2",
            weight=55.5,
            hard=55,
            place="Тагил"
        )
        # добавляем ей только группы
        ins2.groups.add(group)

    def test_color_in_insert_with_color(self):
        """тест объекта у которого есть цвета
        """
        ins = Inserts.objects.get(id=1)
        # Если список пуст, преобразование его к bool даёт False
        self.assertTrue(bool(ins.colors.all()))

    def test_color_in_insert_without_color(self):
        """тест объекта у которого нету цветов
        """
        ins = Inserts.objects.get(id=2)
        # Если список пуст, преобразование его к bool даёт False
        self.assertTrue(bool(ins.colors.all()))


class YourTestCase(TestCase):
    """тесты на доступность пользователю данных по id
    """
    
    @classmethod
    def setUpTestData(cls):
        """инициализируем данные
        создаём 1 объект
        создаём пользователя
        """
        colors = Color.objects.create(
            str_id="666",
            name="test_color"
        )
        group = Group.objects.create(
            str_id="666",
            name="test_group"
        )

        ins = Inserts.objects.create(
            str_id="666",
            name="test1",
            weight=55.5,
            hard=55,
            place="Тагил"
        )
        ins.colors.add(colors)
        ins.groups.add(group)

        user = User.objects.create_user('testuser', password='testuser123')

    def test_read_exist_data(self):
        # данные для авторизации
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Basic ' + base64.b64encode(b'testuser:testuser123').decode('utf-8'),
        }

        c = Client()
        # отправляем запрос
        response = c.get('/insert?id=666', **auth_headers)
        content = response.content.decode('utf-8')
        # если статус 200(Ок) и полученые данные приводяться к Json
        # для более сложной логики, можно было бы ещё проверить содержимое кишков этого Json
        self.assertTrue(response.status_code == 200 and is_json(content))

    def test_read_not_exist_data(self):
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Basic ' + base64.b64encode(b'testuser:testuser123').decode('utf-8'),
        }

        c = Client()
        response = c.get('/insert?id=777', **auth_headers)
        content = response.content.decode('utf-8')
        # если статус 200(Ок) и полученые данные приводяться к Json
        # для более сложной логики, можно было бы ещё проверить содержимое кишков этого Json
        self.assertTrue(response.status_code == 200 and is_json(content))
