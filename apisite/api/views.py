import datetime
import json
import base64

from django.http import HttpResponse, HttpResponseBadRequest
from django.contrib import auth
from django.contrib.auth import authenticate
from api.models import Inserts


def login_or_basic_auth_required(view):
    """Декоратор который оборачивает все функции-вьюхи логикой, которая
    если авторизации нету, заставляет авторизоваться, если авторизация есть, то выполняем тело функции
    """
    def _decorator(request, *args, **kwargs):
        """Стандартная логика basic-auth
        """
        if 'HTTP_AUTHORIZATION' in request.META:
            auth = request.META['HTTP_AUTHORIZATION'].split()
            if len(auth) == 2:
                if auth[0].lower() == "basic":
                    uname, passwd = base64.b64decode(auth[1]).decode('utf-8').split(':')
                    user = authenticate(username=uname, password=passwd)
                    if user is not None and user.is_active:
                        request.user = user
                        return view(request, *args, **kwargs)

        response = HttpResponse()
        response.status_code = 401
        response['WWW-Authenticate'] = 'Basic realm="%s"' % "Basci Auth Protected"
        return response
    return _decorator


def get_data_for_isert(ins):
    """вспомогательная функция, которая по объекту
    Inserts формирует dict с нужным набором данных
    """
    one = {}
    one['id'] = str(ins.id).zfill(3)
    one['name'] = ins.name

    one['groups'] = {}
    for gr in ins.groups.all():
        one['groups'][gr.str_id] = gr.name

    one['weight'] = "{0:.2f}".format(ins.weight)  # - вес в граммах - 2 точки после запятой
    one['hard'] = ins.hard

    one['colors'] = {}
    for color in ins.colors.all():
        one['colors'][color.str_id] = color.name

    one['place'] = ins.place

    return one


@login_or_basic_auth_required
def inserts(request):
    """функция для отображения всех данных в базе
    """
    # получается все объекты Inserts
    ins = list(Inserts.objects.all())
    # подготовительная структура
    rez_json = {
        "meta": {
            "count": len(ins)
        },
        "data": {}
    }
    # пробегам все обекты, формируем результирующий dict
    for i in ins:
        one = get_data_for_isert(i)
        rez_json["data"][ins.index(i)] = one

    # преобразуем результирующий dict к json-строке
    # и формируем Response
    data = json.dumps(rez_json)
    return HttpResponse(data)


@login_or_basic_auth_required
def insert(request):
    """функция для отображения данных о конкретном объекте
       параметр получаем, как параметр GET запроса
    """
    # во первых убеждаемся что это GET-запрос
    if request.method == 'GET':
        # если в параметрах есть id и он число. 001 - тоже число :)
        if 'id' in request.GET and request.GET['id'].isdigit():
            i_id = int(request.GET['id'])
            # ищем конкретный обхект по id
            ins = Inserts.objects.filter(id=i_id)
            # если что-то нашли, формируем Response
            if ins:
                one = get_data_for_isert(ins[0])
                data = json.dumps(one)
                return HttpResponse(data)
    # в случае если с данным где-то была промашка, формируем Response
    # с "жалобой" на плохой запрос
    return HttpResponseBadRequest(
        json.dumps({"error": "incorrect input data"}))